from selenium.webdriver.remote.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By


class BasePage(object):

    def __init__(self, web_driver: WebDriver, url=None):
        self.web_driver = web_driver
        self.url = url

    def get_page(self):
        self.web_driver.get(self.url)

    def title(self):
        return self.web_driver.title

    def is_element_present(self, how, what):
        try:
            self.web_driver.find_element(how, what)

        except NoSuchElementException:
            return False

        return True

    def current_url(self):
        return self.web_driver.current_url

    def find_element(self, how, what, timeout=4):
        element = None

        try:
            element = WebDriverWait(self.web_driver, timeout).until(
                EC.visibility_of_element_located((how, what))
            )
        except NoSuchElementException:
            print(f'Element with locator  not found!')

        return element

    def click_on_element(self, element):

        action = ActionChains(self.web_driver)
        action.move_to_element(element).click(on_element=element).perform()

    def send_keys(self, how, what):

        element = self.find_element(how, what)
        if element:

            keys = 'We Are Anonymous'

            element.click()
            element.clear()
            element.send_keys(keys)

        else:
            msg = 'Element with locator {0} {1} not found'
            raise AttributeError(msg.format(how, what))
