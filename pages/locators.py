from selenium.webdriver.common.by import By


class MainPageLocators:
    URL = 'https://selenium1py.pythonanywhere.com/en-gb/'
    LOGIN_LINK = (By.CSS_SELECTOR, '#login_link')
    INCORRECT_LINK = (By.CSS_SELECTOR, 'incorrect_link')
    BASKET_LINK = (By.CSS_SELECTOR, 'span.btn-group')
    BASKET_TEXT_ELEMENT = (By.CSS_SELECTOR, '#content_inner>p')


class StoreLocators:
    URL = "https://selenium1py.pythonanywhere.com/en-gb/catalogue/"
    MAIN_PAGE_LINK = (By.CSS_SELECTOR, 'div.col-sm-7>a')
    SEARCH_FILED = (By.CSS_SELECTOR, '#id_q')
    SEARCH_BUTTON = (By.CSS_SELECTOR, 'input[type="submit"]')
