from selenium import webdriver
import pytest
from pathlib import Path
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options as chrome_options


def get_chrome_service():
    chrome_driver_path = Path('/home/mkazlou/chromedriver/chromedriver')
    s = Service(chrome_driver_path)
    return s


def get_chrome_options():
    options = chrome_options()
    options.add_argument('--headless')
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-extensions")
    return options


@pytest.fixture(scope="class")
def web_driver():
    service = get_chrome_service()
    options = get_chrome_options()
    web_driver = webdriver.Chrome(options=options, service=service)
    
    yield web_driver
    
    web_driver.quit()

